package com.example.kafka.controller;

import com.example.kafka.model.User;
import com.example.kafka.service.KafkaProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/kafka")
public class KafkaProducerController {
    private KafkaProducer kafkaProducer;

    public KafkaProducerController(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @PostMapping("")
    public ResponseEntity<String> createUser(@RequestBody User user){
        kafkaProducer.sendWelcomeMessage(user);
        return ResponseEntity.ok("Welcome message sent to user");
    }
}
