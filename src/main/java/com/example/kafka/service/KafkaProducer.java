package com.example.kafka.service;

import com.example.kafka.model.User;
import com.example.kafka.utils.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;



@Service
public class KafkaProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendWelcomeMessage(User user){
        String message = String.format("Bienvenido a la aplicacion, %s!", user.getName());
        LOGGER.info(String.format("Enviando mensaje de bienvenida a %s <%s>: %s", user.getName(), user.getEmail(), message));
        kafkaTemplate.send(AppConstants.TOPIC_NAME, message);
    }
}
